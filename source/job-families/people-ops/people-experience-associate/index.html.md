---
layout: job_family_page
title: "People Experience"
---
<a id="associate-requirements"></a>

## People Experience Associate

### Job Grade

The People Experience Associate is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Primary responder to people operations email and Slack channel to ensure extraordinary customer service to team members and leaders at GitLab.
- Administer and coordinate all onboarding tasks and provide assistance as needed.
- Administer and coordinate all offboarding tasks and provide assistance as needed.
- Administer anniversary gifts.
- Administer probation period process - liaising with managers, communicating with PBP’s, updating team members. Manage and update team members' probation period information in BambooHR.
- Work closely with People Operations and broader People team to implement specific People Operations processes and transaction.
- Proactively identify process inefficiencies and inconsistencies and collaborate towards an improved and more productive process that improves the team member and/or manager’s experience.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Use the [Onboarding Satisfaction Survey](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat) (OSAT) results as a guide for future iterations and improving the onboarding experience for new hires - individual feedback can be found in the private onboarding-survey slack channel.
- Administering all transition issues for internal promotions or laterel transfers. Collaborote with the People Analyst team to keep improving the work-flow.
- Handle queries in slack and email relating to different entities and countries.
- Administration of company communication (group conversations, CEO 101 and AMA's).
- Foster collaborative working relationship with the CES team to ensure a new hire has a positive onboarding experience. Escalate any concerns to Senior People Experience Associate.
- Complete ad-hoc projects, reporting, and tasks.
- Apply suitable handover measures to ensure a level of continuity in instances where you may be unable to attend to your regular tasks, particularly those that may impact onboarding, offboarding and transition issues.  The handover discussion should be routed through the People Experience Coordinator for re-allocation purposes.

### Requirements

- The ability to work autonomously and to drive your own performance & development is important in this role.
- Prior extensive experience in a HR or People Operations role.
- Clear understanding of HR laws in one or multiple countries where GitLab is active.
- Willingness to work odd hours when needed (for example, to call an embassy in a different continent).
- Excellent written and verbal communication skills.
- Exceptional customer service skills.
- Strong team player who can jump in and support the team on a variety of topics and tasks.
- Enthusiasm for and broad experience with software tools.
- Proven experience quickly learning new software tools.
- Willing to work with git and GitLab whenever possible.
- Willing to make People Operations as open and transparent as possible.
- Proven organizational skills with high attention to detail and the ability to prioritize.
- You share our [values](/handbook/values), and work in accordance with those values.
- The ability to work in a fast paced environment with strong attention to detail is essential.
- High sense of urgency and accuracy.
- Bachelor's degree or 2 years related experience.
- Experience at a growth-stage tech company is preferred.
- Ability to use GitLab

## Levels

### People Experience Coordinator

#### Job Grade

The People Experience Coordinator is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Review contract to ensure signed and PIAA agreement - escalate information as required.
- Audit referral bonus process.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Flowers and gift ordering.
- First line of contact for all team member enquiries surrounding Anniversary Gifts via slack and / or email.
- Process all employment confirmation, reference, visa and mortgage letters.
- Audit offboarding issues weekly, to ensure People Group tasks were completed.
- Business card & travel system administration.
- Onboarding Support: New hire invites on team calendars, new hire swag administration and adding new hire's to GitLab Unfiltered YouTube channel.
- Build understanding of GitLab employment practices and provide support as required.
- Manage the Team Meetings calendar.
- Complete ad-hoc projects, reporting, and tasks.
- Allocation of tasks to the People Experience Team to ensure even distribution of onboarding, offboarding and transition issues.
- Re-allocation of tasks to ensure a level of continuity in instances where team members have indicated that they may be unable to attend to them by way of a handover discussion.

### Senior People Experience Associate

#### Job Grade

The Senior People Experience Associate is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Ensure all GitLab team members experience extraordinary customer service when engaging with the People Experience Team either via Slack or Email - being mindful of the documented lead times within the People Experience Team SLA (this may from time to time include queries pertaining to specific entities and / or countries).
- Monitor the rotation based tasks facilitated by the People Experience Team while ensuring even distribution of workload and that all tasks are closed out for each cycle i.e. weekly and monthly.
- Oversee the administrative processes surrounding Anniversary Gifts either directly or by way of a team rotation, ensuring emails are updated with the relevant links / codes and sent to all celebrants; making sure adequate stock is available within the warehouse and replenished after each cycle and responding to queries around shipping etc.
- Assist fellow People Experience team members when concerns or questions are raised, identifying suitable solutions and moving blocks or escalating when necessary.
- Continuously review feedback across all formal / informal communication mechanisms i.e. Emails; Slack Channels and Surveys (OSAT or otherwise) to identify trends and suitable opportunities for iteration and improvement of the issues and processes surrounding the People Experience Team.
- Report monthly on trends identified within the OSAT Survey results in addition to suggestions and feedback gathered.
- Administer and coordinate all Onboarding Tasks, maintaining contact with the onboarding team member in addition to other stakeholders such as the Manager; Onboarding Buddy and Tech Provisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer and coordinate all Offboarding Tasks, maintaining contact with the offboarding team member in addition to other stakeholders such as the Manager and Tech Deprovisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Administer and coordinate all Career Mobility Tasks i.e. promotions; demotions or lateral migrations, maintaining contact with the transitioning team member in addition to other stakeholders such as the Manager; Career Mobility Buddy (if one is assigned) and Tech Provisioners to ensure that all tasks are completed while encouraging compliance and offering assistance as and when needed (issue ownership and accountability).
- Collaborate closely with the People Experience Associates and the People Experience Team Lead to ensure Onboarding, Offboarding and Career Mobility issues are able to scale with organisational growth and are continually updated to in alignment with the greater compliance and security requirements of the business.
- Foster partnered relationships with teams and individuals such as the Candidate Experience Specialists (CES) and People Business Partners (PBP) with the intention of supporting urgent / unanticipated Onboardings and Offboardings can be attended to seamlessly.
- Manage the GitLab Team Meetings Calendar either directly or indirectly by way of rotation based task.
- Assist with and complete ad hoc projects, reporting and tasks as directed by the needs of the People Experience Team at any given time.
- Apply suitable handover measures to ensure a level of continuity in instances where you may be unable to attend to your regular tasks, particularly those that may have direct bearing on GitLab team members who may currently be Onboarding or Offboarding.

### Team Lead, People Experience

#### Job Grade

The Team Lead, People Experience is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Coach and mentor People Experience team to effectively represent GitLab's culture and values in all team member interactions
- Bi-weekly People Experience Team meeting to calibrate on weekly priorities.
- Continually audit and monitor compliance with policies and procedures.
- Address any gaps and escalate to Manager, People Operations where required.
- Drive continued automation and efficiency to enhance the employee experience and maintain our efficiency value.
- Announcing changes and improvements in the #whats-happening-at-gitlab Slack channel
- Driving a positive team member experience throughout their lifecycle with GitLab.
- Partner with different teams to ensure a team member is fully offboarded.
- Continually audit offboarding process, identify gaps and address.
- Improve transition issues and partner with L&D to implement training.
- Partner with all teams and hiring managers to ensure a team member is fully onboarded.
- Continually audit onboarding process, identify gaps and address through Issue board.
- Quarterly review of 30 day Onboarding survey.
- Support team on escalated queries / issues.
- Complete ad-hoc projects, reporting, and tasks.

## Performance Indicators

Primary performance indicators for this role:

- [Onboarding Satisfaction Survey > 4.5](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat)
- [Onboarding Task Completion < X (TBD)](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-task-completion--x-tbd)

### Hiring Process
- Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our People Operations Manager
- After that, candidates will be invited to schedule a 30 minute interview with members of the People Operations and People Partner teams
- After that, candidates will be invited to interview with the Director of People Operations
- Finally, our CPO may choose to conduct a final interview

### Career Ladder

The next step in the People Experience job family is to move to the [People Operations](/job-families/people-ops/people-operations/) job family. 
